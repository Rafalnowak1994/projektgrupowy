﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ProjektGrupowy.ViewModels
{
    public class SettingsViewModel
    {

        [Required]
        [DisplayName("Ścieżka do Hangfire'a")]
        public string HangfirePath { get; set; }

        [Required]
        [DisplayName("Nazwa domeny")]
        public string ADDomian { get; set; }

        [Required]
        public string HomeDirectory { get; set; }

        [Required]
        public string HomeDrive { get; set; }

        [Required]
        public string MsSFU30NisDomain { get; set; }

        [Required]
        public string LoginShell { get; set; }

        [Required]
        public int UidNumber { get; set; }

        [Required]
        public string GidNumber { get; set; }

        [Required]
        public string Unixhomedirectory { get; set; }

        [Required]
        [DisplayName("Domyślna nazwa grupy użytkownika")]
        public string DefaultUserGroupName { get; set; }

        [Required]
        [DisplayName("Nazwa grupy z dostępem do zmiany ustawień")]
        public string ChangeSettingsGroup { get; set; }

        [Required]
        [DisplayName("Nazwa grupy z dostępem do wysyłania tokenów")]
        public string SendTokensGroup { get; set; }

        [Required]
        [DisplayName("Nazwa grupy z dostępem do edycji użytkowników")]
        public string EditUsersGroup { get; set; }

        [Required]
        [DisplayName("Nazwa grupy z dostępem do aktywacji użytkowników zakładających konto bez tokena")]
        public string ActivateUsersFromDataBaseGroup { get; set; }

        [Required]
        [DisplayName("Opis")]
        public string Descripton { get; set; }

        [Required]
        [DisplayName("Profile Path")]
        public string ProfilePath { get; set; }

    

        [DisplayName("Adres email na który bedą wysyłane informacje o nowych użytkownikach")]
        [EmailAddress]
        public string InfoMail { get; set; }
    }
}