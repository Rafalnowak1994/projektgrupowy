﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.ViewModels
{
    public class DataGridsViewModels
    {
        public class TokenGroupDataTable
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Date { get; set; }
        }
        public class UsersTableViewModel
        {
            public string Id { get; set; }
            public string Login { get; set; }
            public string Email { get; set; }
            public string Description { get; set; }
            public string GIODO { get; set; }
            public string Expire { get; set; }
        }
        public class UsersToActivateFromDataBaseViewModel
        {
            public string Id { get; set; }
            public string Index { get; set; }
            public string Date { get; set; }
            public string Email { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string Description { get; set; }
        }
    }
}