﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.ViewModels
{
    public class EmailsTokenViewModel
    {
        [Required]
        [DisplayName("Adresy email")]
        public string Mails { get; set; }

        [Required]
        [DisplayName("Ważność tokena (dni)")]
        public int Days { get; set; }
    }
}