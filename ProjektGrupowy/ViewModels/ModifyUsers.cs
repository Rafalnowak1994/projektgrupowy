﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.ViewModels
{
    public class ModifyUsersViewModel
    {
     
        [DisplayName("Home Directory")]
        public string HomeDirectory { get; set; }
    
        [DisplayName("Home Drive")]
        public string HomeDrive { get; set; }
       
        [DisplayName("MsSFU30NisDomain")]
        public string MsSFU30NisDomain { get; set; }
   
        [DisplayName("LoginShell")]
        public string LoginShell { get; set; }
       
        [DisplayName("GidNumber")]
        public string GidNumber { get; set; }
   
        [DisplayName("Unixhomedirectory")]
        public string Unixhomedirectory { get; set; }

        public string[] Logins {get; set;}
    }
}