﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.ViewModels
{
    public class ChangePasswordViewModel
    {
        [Required]
        [RegularExpression(@"((?=.*\d)(?=.*[A-Z]).{7,20})", ErrorMessage = "Hasło musi zawierać od 7 do 20 znaków i składać się z dużej litery oraz liczby")]
        [DataType(DataType.Password)]
        [DisplayName("Nowe hasło")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Hasła nie są jednakowe")]
        [DisplayName("Potwierdź nowe hasło")]
        public string ConfirmNewPassword { get; set; }
    }
}