﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.ViewModels
{
    public class ChangeDataViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Name is too short or too long")]
        [DisplayName("Imię")]
        public string Name { get; set; }
        [Required]
        [StringLength(45, MinimumLength = 3, ErrorMessage = "Surname is too short or too long")]
        [DisplayName("Nazwisko")]
        public string Surname { get; set; }

        public bool GIODO { get; set; }
        public bool Newsletter { get; set; }

    }
}