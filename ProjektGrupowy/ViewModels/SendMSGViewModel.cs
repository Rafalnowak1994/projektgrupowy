﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjektGrupowy.ViewModels
{
    public class SendMSGViewModel
    {
        public string subject { get; set; }
        public string message { get; set; }
        public string[] Emails { get; set;}
    }
}