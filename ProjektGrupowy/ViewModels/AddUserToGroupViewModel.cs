﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjektGrupowy.ViewModels
{
    public class AddUserToGroupViewModel 
    {
        public string ADGroups { get; set; }
        public string[] Logins { get; set; }
     }
}