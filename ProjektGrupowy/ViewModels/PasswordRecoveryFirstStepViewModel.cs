﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.ViewModels
{
    public class PasswordRecoveryFirstStepViewModel
    {
        [Required]
        [DisplayName("Nazwa użytkownika")]
        public string Login { get; set; }
    }
}