﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjektGrupowy.ViewModels
{
    public class LocalLoginViewModel
    {

        [DisplayName("Nazwa użytkownika")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Hasło")]
        public string Password { get; set; }
    }
}