﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.ViewModels
{
    public class TokenGroupGenerateViewModel
    {
        [Required]
        [DisplayName("Ilość tokenów")]
        [Range(1, int.MaxValue, ErrorMessage = "The value must be greater than 0")]
        public int Amount { get; set; }

        [Required]
        [DisplayName("Nazwa grupy tokenów")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Ważność tokena (dni)")]
        [Range(1, int.MaxValue, ErrorMessage = "The value must be greater than 0")]
        public int Days { get; set; }
    }
}