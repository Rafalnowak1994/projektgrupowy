﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.ViewModels
{
    public class FirstConfigViewModel
    {
        [Required]
        [DisplayName("Nazwa użytkownika")]
        public string Username { get; set; }

        [Required]
        [DisplayName("Hasło")]
        [DataType(DataType.Password)]
        [RegularExpression(@"((?=.*\d)(?=.*[A-Z]).{7,20})", ErrorMessage = "Hasło musi zawierać od 7 do 20 znaków i składać się z dużej litery oraz liczby")]

        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Hasła nie są jednakowe")]
        [DisplayName("Potwierdź hasło")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DisplayName("Domena ActiveDirectory")]
        public string ADDomian { get; set; }
    }

}