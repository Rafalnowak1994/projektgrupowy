﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.ViewModels
{
    public class CreateAccountViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Name is too short or too long")]
        [DisplayName("Imię")]
        public string Name { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Surname is too short too long")]
        [DisplayName("Nazwisko")]
        public string Surname { get; set; }

        [Required]
        [DisplayName("Numer indeksu")]
        [Range(0, Int32.MaxValue)]
        public int Index { get; set; }

        [DisplayName("Token")]
        public string Token { get; set; }

        [Required]
        public bool GIODO { get; set; }
        public bool Newsletter { get; set; }


        [DisplayName("Mam token")]
        public bool HaveToken { get; set; }

    }
}