﻿using MySql.Data.Entity;
using ProjektGrupowy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.DAL
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class MySQLContext : DbContext
    {
        public DbSet<UserRegisterToken> UserRegisterTokens
        {
            get;
            set;
        }


        public DbSet<TokenGroup> TokenGroups
        {
            get;
            set;
        }

        public DbSet<PasswordRecoveryToken> PasswordRecoveryTokens
        {
            get;
            set;
        }

        public DbSet<User> Users
        {
            get;
            set;
        }

        public DbSet<Log> Logs
        {
            get;
            set;
        }
        public MySQLContext()  
        //Reference the name of your connection string  
        : base("MySQLContext") { }
    }
}