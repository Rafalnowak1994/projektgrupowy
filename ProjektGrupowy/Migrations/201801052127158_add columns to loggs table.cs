namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcolumnstologgstable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs", "Date", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Logs", "Date");
        }
    }
}
