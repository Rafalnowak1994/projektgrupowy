namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addloggstable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Action = c.String(unicode: false),
                        Controler = c.String(unicode: false),
                        Method = c.String(unicode: false),
                        Url = c.String(unicode: false),
                        UserAgent = c.String(unicode: false),
                        Level = c.String(unicode: false),
                        Message = c.String(unicode: false),
                        Exception = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Logs");
        }
    }
}
