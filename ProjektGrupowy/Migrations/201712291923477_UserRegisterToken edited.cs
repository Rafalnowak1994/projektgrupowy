namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserRegisterTokenedited : DbMigration
    {
        public override void Up()
        {
            AlterColumn("UserRegisterTokens", "Token", c => c.String(nullable: false, maxLength: 128, storeType: "nvarchar"));
            DropPrimaryKey("UserRegisterTokens");
            AddPrimaryKey("UserRegisterTokens", "Token");
            DropColumn("UserRegisterTokens", "Email");
        }
        
        public override void Down()
        {
            AddColumn("UserRegisterTokens", "Email", c => c.String(nullable: false, maxLength: 128, storeType: "nvarchar"));
            DropPrimaryKey("UserRegisterTokens");
            AddPrimaryKey("UserRegisterTokens", "Email");
            AlterColumn("UserRegisterTokens", "Token", c => c.String(unicode: false));
        }
    }
}
