namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedRequiredfieldstoUsers : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Users", "Name", c => c.String(nullable: false, unicode: false));
            AlterColumn("Users", "Surname", c => c.String(nullable: false, unicode: false));
            AlterColumn("Users", "Password", c => c.String(nullable: false, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("Users", "Password", c => c.String(unicode: false));
            AlterColumn("Users", "Surname", c => c.String(unicode: false));
            AlterColumn("Users", "Name", c => c.String(unicode: false));
        }
    }
}
