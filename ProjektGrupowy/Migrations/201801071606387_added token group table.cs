namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedtokengrouptable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TokenGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Expire = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.UserRegisterTokens", "GroupId", c => c.Int());
            CreateIndex("dbo.UserRegisterTokens", "GroupId");
            AddForeignKey("dbo.UserRegisterTokens", "GroupId", "dbo.TokenGroups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRegisterTokens", "GroupId", "dbo.TokenGroups");
            DropIndex("dbo.UserRegisterTokens", new[] { "GroupId" });
            DropColumn("dbo.UserRegisterTokens", "GroupId");
            DropTable("dbo.TokenGroups");
        }
    }
}
