namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deletedpasswordrecoverytokentable : DbMigration
    {
        public override void Up()
        {
            DropTable("PasswordRecoveryTokens");
        }
        
        public override void Down()
        {
            CreateTable(
                "PasswordRecoveryTokens",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Token = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Email)                ;
            
        }
    }
}
