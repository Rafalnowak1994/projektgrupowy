namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editeduserstable : DbMigration
    {
        public override void Up()
        {
            AddColumn("Users", "Confirmed", c => c.Boolean(nullable: false));
            DropColumn("Users", "Password");
        }
        
        public override void Down()
        {
            AddColumn("Users", "Password", c => c.String(nullable: false, unicode: false));
            DropColumn("Users", "Confirmed");
        }
    }
}
