namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedUsertable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Users",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Name = c.String(unicode: false),
                        Surname = c.String(unicode: false),
                        Password = c.String(unicode: false),
                        GIODO = c.Boolean(nullable: false),
                        Newsletter = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Email)                ;
            
        }
        
        public override void Down()
        {
            DropTable("Users");
        }
    }
}
