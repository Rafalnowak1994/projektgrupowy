namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class passwordRecoveryTokentable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PasswordRecoveryTokens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        User = c.String(nullable: false, unicode: false),
                        Token = c.String(nullable: false, unicode: false),
                        Expire = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PasswordRecoveryTokens");
        }
    }
}
