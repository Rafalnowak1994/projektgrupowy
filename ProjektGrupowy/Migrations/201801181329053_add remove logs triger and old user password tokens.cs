namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addremovelogstrigerandolduserpasswordtokens : DbMigration
    {
        public override void Up()
        {
            Sql(@"CREATE EVENT deleteOldLogs ON SCHEDULE EVERY 1 WEEK DO DELETE FROM logs WHERE (logs.Date <  DATE(NOW() - INTERVAL 14 DAY)) ;");
            Sql(@"CREATE EVENT deleteOldPasswordRecoveryTokens ON SCHEDULE EVERY 1 WEEK DO DELETE FROM passwordrecoverytokens WHERE passwordrecoverytokens.Expire <  NOW() ;");
        }
        
        public override void Down()
        {
           Sql("DROP EVENT deleteOldLogs");
           Sql("DROP EVENT deleteOldPasswordRecoveryTokens");
        }
    }
}
