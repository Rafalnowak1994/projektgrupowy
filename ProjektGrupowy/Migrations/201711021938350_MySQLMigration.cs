namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MySQLMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PasswordRecoveries",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Token = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Email);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PasswordRecoveries");
        }
    }
}
