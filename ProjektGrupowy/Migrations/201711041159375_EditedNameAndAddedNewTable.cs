namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedNameAndAddedNewTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "PasswordRecoveries", newName: "PasswordRecoveryTokens");
            CreateTable(
                "UserRegisterTokens",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Token = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Email)                ;
            
        }
        
        public override void Down()
        {
            DropTable("UserRegisterTokens");
            RenameTable(name: "PasswordRecoveryTokens", newName: "PasswordRecoveries");
        }
    }
}
