namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deleteduserstable : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Users");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Index = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false, precision: 0),
                        Name = c.String(nullable: false, unicode: false),
                        Surname = c.String(nullable: false, unicode: false),
                        GIODO = c.Boolean(nullable: false),
                        Newsletter = c.Boolean(nullable: false),
                        Confirmed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.Email, t.Index });
            
        }
    }
}
