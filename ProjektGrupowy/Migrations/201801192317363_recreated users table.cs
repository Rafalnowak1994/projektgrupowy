namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class recreateduserstable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, unicode: false),
                        Index = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false, precision: 0),
                        Name = c.String(nullable: false, unicode: false),
                        Surname = c.String(nullable: false, unicode: false),
                        GIODO = c.Boolean(nullable: false),
                        Newsletter = c.Boolean(nullable: false),
                        Confirmed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
        }
    }
}
