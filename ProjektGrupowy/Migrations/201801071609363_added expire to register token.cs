namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedexpiretoregistertoken : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserRegisterTokens", "Expire", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserRegisterTokens", "Expire");
        }
    }
}
