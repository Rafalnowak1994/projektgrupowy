namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class addedgrouptrigger : DbMigration
    {
        public override void Up()
        {
            Sql(@"CREATE EVENT deleteOldTokensFromTokenGroups ON SCHEDULE EVERY 1 WEEK DO DELETE FROM tokengroups WHERE tokengroups.Expire < NOW() ;");
        }

        public override void Down()
        {
            Sql("DROP EVENT deleteOldTokensFromTokenGroups");
        }
    }
}
