namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtrigger : DbMigration
    {
        public override void Up()
        {
            Sql(@"CREATE EVENT deleteOldTokensFromUserRegisterTokens ON SCHEDULE EVERY 1 WEEK DO 
DELETE FROM userregistertokens where userregistertokens.Expire <  NOW() ;");
        }
        
        public override void Down()
        {
            this.Sql("DROP EVENT deleteOldTokensFromUserRegisterTokens");
        }
    }
}
