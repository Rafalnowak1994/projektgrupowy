namespace ProjektGrupowy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedindexfieldtoUsers : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("Users");
            AddColumn("Users", "Index", c => c.Int(nullable: false));
            AddPrimaryKey("Users", new[] { "Email", "Index" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("Users");
            DropColumn("Users", "Index");
            AddPrimaryKey("Users", "Email");
        }
    }
}
