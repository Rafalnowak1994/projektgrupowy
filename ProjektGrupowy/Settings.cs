﻿using NLog;
using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Web;

namespace ProjektGrupowy
{
    public static class Settings
    {

        static Configuration objConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
        static AppSettingsSection objAppsettings = (AppSettingsSection)objConfig.GetSection("appSettings");
        static private Logger logger = LogManager.GetCurrentClassLogger();

        public const string SuperAdminGroupNameLocal = "SuperAdminLocal";

        private const string notAllowedDomainName = "Domain Users";
        private const string notAllowedDomainName1 = "Użytkownicy domeny";

        //captcha 
        public const string reCAPTCHASecretKey = "6Le35z4UAAAAAHq39bJjKbRQwsWtrwlUTrt-cay7";
        public const string data_sitekey = "6Le35z4UAAAAAKwlpd8mvDwN913ilTZWPjz1g11X";


        public static string ShowConfigSite
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["ShowConfigSite"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["ShowConfigSite"];
            }
        }

        public static string SuperAdminLogin
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["SuperAdminLogin"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["SuperAdminLogin"];
            }
        }

        public static string ActivateUsersFromDataBaseGroup
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["ActivateUsersFromDataBaseGroup"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["ActivateUsersFromDataBaseGroup"];
            }
        }

        public static string SuperAdminPassword
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["SuperAdminPassword"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["SuperAdminPassword"];
            }
        }

        public static string ADDomian
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["ADDomian"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["ADDomian"];
            }
        }

        public static string HangfirePath
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["HangfirePath"].Value = value;
                    objConfig.Save();
                }

            }
            get
            {
                return ConfigurationManager.AppSettings["HangfirePath"];
            }
        }


        public static string ChangeSettingsGroup
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["ChangeSettingsGroup"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["ChangeSettingsGroup"];
            }
        }

        public static string InfoMail
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["InfoMail"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["InfoMail"];
            }
        }

        public static string SendTokensGroup
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["SendTokensGroup"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["SendTokensGroup"];
            }
        }

        public static string EditUsersGroup
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["EditUsersGroup"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["EditUsersGroup"];
            }
        }
        public static string HomeDirectory
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["HomeDirectory"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["HomeDirectory"];
            }


        }
        public static string HomeDrive
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["HomeDrive"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["HomeDrive"];
            }


        }
        public static string MsSFU30NisDomain
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["MsSFU30NisDomain"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["MsSFU30NisDomain"];
            }

        }
        public static string LoginShell
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["LoginShell"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["LoginShell"];
            }

        }

        public static string GidNumber
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["GidNumber"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["GidNumber"];
            }


        }
        public static string Unixhomedirectory
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["Unixhomedirectory"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["Unixhomedirectory"];
            }

        }

        public static int UidNumber
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["UidNumber"].Value = value.ToString();
                    objConfig.Save();
                }
            }
            get
            {
                return Int32.Parse(ConfigurationManager.AppSettings["UidNumber"]);
            }

        }
        public static string DefaultUserGroupName
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["DefaultUserGroupName"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["DefaultUserGroupName"];
            }

        }
        public static string Description
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["Description"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["Description"];
            }
        }
        public static string ProfilePath
        {
            set
            {
                if (objAppsettings != null)
                {
                    objAppsettings.Settings["ProfilePath"].Value = value;
                    objConfig.Save();
                }
            }
            get
            {
                return ConfigurationManager.AppSettings["ProfilePath"];
            }
        }
        public static string NotAllowedDomainName => notAllowedDomainName;

        public static string NotAllowedDomainName1 => notAllowedDomainName1;



        public static PrincipalContext GetPrincipalContext()
        {
            try
            {
                return new PrincipalContext(ContextType.Domain, ADDomian);
            }

            catch (Exception e)
            {
                logger.Fatal(e, "Błąd połączenia z usługą Active Directory");
                throw new HttpException("500"); 
            }
        }
    }
}