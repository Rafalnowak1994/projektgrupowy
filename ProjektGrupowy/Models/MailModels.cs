﻿using Postal;
using ProjektGrupowy.Models;

namespace ProjektGrupowy.Models
{
    public class PasswordRecoverTokenMail : Email
    {
        public string To { get; set; }
        public string Login { get; set; }
        public string Url { get; set; }
    }

    public class TokenToCreateAccount : Email
    {
        public string To { get; set; }
        public string Token { get; set; }
    }
    public class SendMessage : Email
    {
        public string To { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
    }
}