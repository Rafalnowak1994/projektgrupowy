﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.Models
{
    public class User
    {
        [Required]
        [Key]
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public int Index { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public bool GIODO { get; set; }

        [Required]
        public bool Newsletter { get; set; }

        [Required]
        public bool Confirmed { get; set; }
    }
}