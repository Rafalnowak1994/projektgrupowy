﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.Models
{
    public class TokenGroup
    {
   
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        [Required]
        public DateTime Expire { get; set; }

        public ICollection<UserRegisterToken> Tokens { get; set; }
    }
}