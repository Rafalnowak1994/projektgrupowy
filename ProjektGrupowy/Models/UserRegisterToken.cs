﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.Models
{
    public class UserRegisterToken
    {
        [Key]
        public string Token { get; set; }

        [ForeignKey("Group")]
        public int ? GroupId { get; set; }

        [Required]
        public DateTime Expire { get; set; }

        public TokenGroup Group { get; set; }
    }
}