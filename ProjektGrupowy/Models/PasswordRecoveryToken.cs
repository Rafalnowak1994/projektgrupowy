﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.Models
{
    public class PasswordRecoveryToken
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string User { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        public DateTime Expire { get; set; }

    }
}