﻿using ProjektGrupowy.DAL;
using ProjektGrupowy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.Infrastructure
{
    public class MailSender
    {
        private static MySQLContext mySQLContext;

        public static void sendNewTokenToPasswordRecovery(string url, string userEmail, string login)
        {
            PasswordRecoverTokenMail email = new PasswordRecoverTokenMail();
            email.To = userEmail;
            email.Login = login;
            email.Url = url;
            email.Send();
        }

        public static void sendActivateTokenToEmail(string token, string userEmail)
        {
            TokenToCreateAccount email = new TokenToCreateAccount();
            email.To = userEmail;
            email.Token = token;
            email.Send();
        }

        public static void addTokenToDataBaseAndSend(string email, DateTime date)
        {
            using (mySQLContext = new MySQLContext())
            {
                string token = ADUserMethods.GenerateToken();
                mySQLContext.UserRegisterTokens.Add(new UserRegisterToken { Token = token, Expire = date });
                mySQLContext.SaveChanges();
                sendActivateTokenToEmail(token, email.Trim());
            }
        }

        public static void sendMessage (string adress, string subject, string message)
        {
            SendMessage email = new SendMessage();
            email.To = adress;
            email.Subject = subject;
            email.Message = message;
            email.Send();
        }
    }

}