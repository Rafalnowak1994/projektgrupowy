﻿using Hangfire;
using NLog;
using ProjektGrupowy.DAL;
using ProjektGrupowy.Models;
using ProjektGrupowy.ViewModels;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;

namespace ProjektGrupowy.Infrastructure
{
    public static class ADUserMethods
    {
        static Logger logger = LogManager.GetCurrentClassLogger();
        static Random random = new Random();
        private static MySQLContext mySQLContext;


        public static bool ChangePassword(string password, string token)
        {
            PasswordRecoveryToken passwordRecoverytoken = null;
            try
            {
                using (mySQLContext = new MySQLContext())
                {
                    passwordRecoverytoken = mySQLContext.PasswordRecoveryTokens.Where(w => w.Token == token).FirstOrDefault();

                    if (passwordRecoverytoken != null)
                    {
                        mySQLContext.PasswordRecoveryTokens.Remove(passwordRecoverytoken);
                        using (PrincipalContext principalContext = Settings.GetPrincipalContext())
                        {
                            using (UserPrincipal user = UserPrincipal.FindByIdentity(principalContext, passwordRecoverytoken.User))
                            {
                                user.SetPassword(password);
                                user.Enabled = true;
                                user.Save();
                            }
                        }
                        mySQLContext.SaveChanges();
                        logger.Info($"Zmieniono hasło dla użytkowanika {passwordRecoverytoken.User}");
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                logger.Fatal(e, $"Wystąpił błąd zmiany hasła dla użytkownika {passwordRecoverytoken?.User ?? ""}");
                return false;
            }
            return false;
        }

        public static string AddPasswordRecoveryTokenToDataBase(string user)
        {
            string token = GenerateToken();
            try
            {
                using (mySQLContext = new MySQLContext())
                {
                    var lastToken = mySQLContext.PasswordRecoveryTokens.Where(w => w.User == user).FirstOrDefault();

                    if (lastToken != null)
                    {
                        mySQLContext.PasswordRecoveryTokens.Remove(lastToken);
                    }

                    mySQLContext.PasswordRecoveryTokens.Add(new Models.PasswordRecoveryToken
                    {
                        User = user,
                        Token = token,
                        Expire = DateTime.Now.AddDays(1)
                    });

                    mySQLContext.SaveChanges();
                }

                logger.Info($"Dodano token do zmiany hasła dla użytkownika {user}");
            }
            catch (Exception e)
            {
                logger.Fatal(e, $"Wystąpił błąd przy dodawaniu tokena zmiany hasła użytkownika {user}");
                return "";
            }
            return token;
        }

        public static bool addUserToActiveDirectory(CreateAccountViewModel model)
        {
            try
            {
                using (PrincipalContext principalContext = Settings.GetPrincipalContext())
                {

                    // Create the new UserPrincipal object
                    UserPrincipal user = new UserPrincipal(principalContext, $"s{model.Index}", GenerateRandomPassword(), true);
                    user.UnlockAccount();
                    user.Surname = model.Surname;
                    user.GivenName = model.Name;
                    user.Description += Settings.Description;
                    
                    user.EmailAddress = model.Email;
                    user.UserPrincipalName = "s" + model.Index + $"@{Settings.ADDomian}";
                    
                    user.HomeDirectory = Settings.HomeDirectory + user.SamAccountName;
                    user.HomeDrive = Settings.HomeDrive;
                    DateTime dateTime = DateTime.Today.AddMonths(18);
                    user.AccountExpirationDate = dateTime;
                    user.Enabled = false;
                    
                    
                    user.Save();

                    DirectoryEntry entry = (DirectoryEntry)user.GetUnderlyingObject();

                    entry.Properties["profilepath"].Value = Settings.ProfilePath+user.SamAccountName;
                    entry.Properties["postalCode"].Value += model.GIODO ? "GIODO:1;" : "GIODO:0;";
                    entry.Properties["postalCode"].Value += model.Newsletter ? "Newsletter:1;" : "Newsletter:0;";
                    entry.Properties["msSFU30NisDomain"].Value = Settings.MsSFU30NisDomain;
                    entry.Properties["LoginShell"].Value = Settings.LoginShell;
                    entry.Properties["uidNumber"].Value = Settings.UidNumber;
                    entry.Properties["gidNumber"].Value = Settings.GidNumber;
                    entry.Properties["unixhomedirectory"].Value = Settings.Unixhomedirectory + user.SamAccountName;

                    entry.CommitChanges();
                    foreach (var item in Settings.DefaultUserGroupName.Split(','))
                    {
                        string groupName = item.Trim();
                        if (Settings.NotAllowedDomainName != groupName && Settings.NotAllowedDomainName1 != groupName)
                        {
                            GroupPrincipal group = GroupPrincipal.FindByIdentity(principalContext, IdentityType.Name, groupName);

                            if (group != null)
                            {
                                group.Members.Add(principalContext, IdentityType.UserPrincipalName, user.UserPrincipalName);
                                group.Save();
                            }
                            else
                            {
                                logger.Fatal("Wystąpił błąd przy tworzeniu konta użytkownika. Nie znaleziono grupy");
                                return false;
                            }
                        }
                    }
               
                    Settings.UidNumber++;


                    logger.Info($"Dodano nowego użytkownika {user.SamAccountName}");
                    if (Settings.InfoMail != null && Settings.InfoMail != "")
                    {//send email in background
                        BackgroundJob.Enqueue(() => MailSender.sendMessage(Settings.InfoMail, $"Nowy uzytkownik", $"Konto uzytkownika s{model.Index} zostalo dodane do Active Directory"));
                    }
                }
            }
            catch (Exception e)
            {
                logger.Fatal(e, "Wystąpił błąd przy tworzeniu konta użytkownika" + e);
                return false;
            }
            return true;
        }

        public static string GenerateRandomPassword()
        {
            string password;

            string bigChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            password = new string(Enumerable.Repeat(bigChars, 3).Select(s => s[random.Next(s.Length)]).ToArray());

            string lowerChars = "abcdefghijklmnopqrstuvwxyz";
            password += new string(Enumerable.Repeat(lowerChars, 3).Select(s => s[random.Next(s.Length)]).ToArray());

            string digits = "0123456789";
            password += new string(Enumerable.Repeat(digits, 2).Select(s => s[random.Next(s.Length)]).ToArray());

            string signs = "!@#$%";
            password += new string(Enumerable.Repeat(signs, 2).Select(s => s[random.Next(s.Length)]).ToArray());

            return new string(password.ToCharArray().
                OrderBy(s => (random.Next(2) % 2) == 0).ToArray());
        }

        public static string GenerateToken()
        {
            string token;

            string bigChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            token = new string(Enumerable.Repeat(bigChars, 3).Select(s => s[random.Next(s.Length)]).ToArray());

            string lowerChars = "abcdefghijklmnopqrstuvwxyz";
            token += new string(Enumerable.Repeat(lowerChars, 3).Select(s => s[random.Next(s.Length)]).ToArray());

            string digits = "0123456789";
            token += new string(Enumerable.Repeat(digits, 3).Select(s => s[random.Next(s.Length)]).ToArray());

            string signs = "!@#$%";
            token += new string(Enumerable.Repeat(signs, 3).Select(s => s[random.Next(s.Length)]).ToArray());


            token = new string(token.ToCharArray().
                OrderBy(s => (random.Next(2) % 2) == 0).ToArray());

            return token;
        }
    }
}