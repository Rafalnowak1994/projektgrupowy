﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Web;
using ProjektGrupowy.Infrastructure;
using System.Threading.Tasks;
using System.DirectoryServices;
using NLog;

namespace ProjektGrupowy.Infrastructure
{
    public class ADAuthenticationService
    {
       public class AuthenticationResult
        {
            public AuthenticationResult(string errorMessage = null)
            {
                ErrorMessage = errorMessage;
            }

            public String ErrorMessage { get; private set; }
            public Boolean IsSuccess => String.IsNullOrEmpty(ErrorMessage);
        }

        private readonly IAuthenticationManager authenticationManager;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public ADAuthenticationService(IAuthenticationManager authenticationManager)
        {
            this.authenticationManager = authenticationManager;
        }

        /// <summary>
        /// Check if username and password matches existing account in AD. 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public AuthenticationResult SignIn(String username, String password)
        {

            using (var foundUser = UserPrincipal.FindByIdentity(Settings.GetPrincipalContext(), IdentityType.SamAccountName, username))
            {
                if (foundUser != null && foundUser.Enabled == false)
                {
                    return new AuthenticationResult("Konto jest nieaktywne");
                }

            }

            bool isAuthenticated = false;
            UserPrincipal userPrincipal = null;
            try
            {
                using (PrincipalContext principalContext = Settings.GetPrincipalContext())
                {
                    isAuthenticated = IsAuthenticated(username, password);
                    if (isAuthenticated)
                    {
                        userPrincipal = UserPrincipal.FindByIdentity(principalContext, username);
                    }

                    if (!isAuthenticated || userPrincipal == null)
                    {
                        return new AuthenticationResult("Nieprawidłowy login lub hasło");
                    }

                    if (userPrincipal.IsAccountLockedOut())
                    {
                        // here can be a security related discussion weather it is worth 
                        // revealing this information
                        return new AuthenticationResult("Konto jest zablokowane");
                    }

                    var identity = CreateIdentity(userPrincipal);

                    authenticationManager.SignOut(MyAuthentication.ApplicationCookie);
                    authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, identity);
                }
            }
            catch (Exception e)
            {
                logger.Fatal($"Błąd logowania {e}");
                isAuthenticated = false;
                userPrincipal = null;
            }

            return new AuthenticationResult();
        }


        private ClaimsIdentity CreateIdentity(UserPrincipal userPrincipal)
        {
            var identity = new ClaimsIdentity(MyAuthentication.ApplicationCookie, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            identity.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "Active Directory"));
            identity.AddClaim(new Claim(ClaimTypes.Name, userPrincipal.SamAccountName));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userPrincipal.SamAccountName));
            if (!String.IsNullOrEmpty(userPrincipal.EmailAddress))
            {
                identity.AddClaim(new Claim(ClaimTypes.Email, userPrincipal.EmailAddress));
               
            }
            try
            {
                var groups = userPrincipal.GetAuthorizationGroups();

                foreach (var @group in groups)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, @group.Name));
                }
            }
            catch (Exception e)
            {
                logger.Fatal($"Błąd zapytania o grupy użytkownika {e}");
            }
            return identity;
        }

        private bool IsAuthenticated(string username, string pwd)
        {
            try
            {
                using (var context = Settings.GetPrincipalContext())
                {
                    return context.ValidateCredentials(username, pwd);
                }
            }
            catch
            {
                return false;
            }
        }
    }
}