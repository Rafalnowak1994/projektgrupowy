﻿using ProjektGrupowy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using static ProjektGrupowy.ViewModels.DataGridsViewModels;

namespace ProjektGrupowy.Infrastructure
{
    public class DataTableResultSet
    {
        public List<UsersTableViewModel> GetResult(string search, string sortOrder, int start, int length, List<UsersTableViewModel> dtResult, List<string> columnFilters)
        {
            if (length != -1)
                return FilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length).ToList();
            else
                return FilterResult(search, dtResult, columnFilters).SortBy(sortOrder).ToList();
        }

        public int Count(string search, List<UsersTableViewModel> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).Count();
        }

        private IQueryable<UsersTableViewModel> FilterResult(string search, List<UsersTableViewModel> dtResult, List<string> columnFilters)
        {
            IQueryable<UsersTableViewModel> results = dtResult.AsQueryable();

            results = results.Where(p => (search == null || (
            p.Login != null && p.Login.ToLower().Contains(search.ToLower()) 
            || p.Email != null && p.Email.ToLower().Contains(search.ToLower())
            || p.Description != null && p.Description.ToLower().Contains(search.ToLower())
            || p.Expire != null && p.Expire.ToLower().Contains(search.ToLower())))
            && (columnFilters[1] == null || (p.Login != null && p.Login.ToLower().Contains(columnFilters[1].ToLower())))
            && (columnFilters[2] == null || (p.Email != null && p.Email.ToLower().Contains(columnFilters[2].ToLower())))
            && (columnFilters[3] == null || (p.Description != null && p.Description.ToLower().Contains(columnFilters[3].ToLower())))
            && (columnFilters[4] == null || (p.GIODO != null && p.GIODO.ToLower().Contains(columnFilters[4].ToLower())))
            && (columnFilters[5] == null || (p.Expire != null && p.Expire.ToLower().Contains(columnFilters[5].ToLower())))
            );

            return results;
        }


        public int Count(string search, List<UsersToActivateFromDataBaseViewModel> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).Count();
        }

        public List<UsersToActivateFromDataBaseViewModel> GetResult(string search, string sortOrder, int start, int length, List<UsersToActivateFromDataBaseViewModel> dtResult, List<string> columnFilters)
        {
            if (length != -1)
                return FilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length).ToList();
            else
                return FilterResult(search, dtResult, columnFilters).SortBy(sortOrder).ToList();
        }

        private IQueryable<UsersToActivateFromDataBaseViewModel> FilterResult(string search, List<UsersToActivateFromDataBaseViewModel> dtResult, List<string> columnFilters)
        {
            IQueryable<UsersToActivateFromDataBaseViewModel> results = dtResult.AsQueryable();

            results = results.Where(p => (search == null || (p.Index != null && p.Index.ToLower().Contains(search.ToLower()) || p.Email != null && p.Email.ToLower().Contains(search.ToLower()) 
            || p.Name != null && p.Name.ToLower().Contains(search.ToLower()) || p.Surname != null && p.Surname.ToLower().Contains(search.ToLower()) || p.Description != null && p.Description.ToLower().Contains(search.ToLower())
            || p.Date != null && p.Date.ToLower().Contains(search.ToLower())))
                && (columnFilters[1] == null || (p.Index != null && p.Index.ToLower().Contains(columnFilters[1].ToLower())))
                && (columnFilters[2] == null || (p.Email != null && p.Email.ToLower().Contains(columnFilters[2].ToLower())))
                && (columnFilters[3] == null || (p.Name != null && p.Name.ToLower().Contains(columnFilters[3].ToLower())))
                && (columnFilters[4] == null || (p.Surname != null && p.Surname.ToLower().Contains(columnFilters[4].ToLower())))
                && (columnFilters[5] == null || (p.Description != null && p.Description.ToLower().Contains(columnFilters[5].ToLower())))
                && (columnFilters[6] == null || (p.Date != null && p.Date.ToLower().Contains(columnFilters[6].ToLower())))
                );

            return results;
        }
    }
}