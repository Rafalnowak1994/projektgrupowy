﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace ProjektGrupowy.Infrastructure
{
    public class LocalAuthenticationService
    {
        bool isAuthenticated = false;

        public class AuthenticationResult
        {
            public AuthenticationResult(string errorMessage = null)
            {
                ErrorMessage = errorMessage;
            }

            public String ErrorMessage { get; private set; }
            public Boolean IsSuccess => String.IsNullOrEmpty(ErrorMessage);
        }

        private readonly IAuthenticationManager authenticationManager;

        public LocalAuthenticationService(IAuthenticationManager authenticationManager)
        {
            this.authenticationManager = authenticationManager;
        }

        public AuthenticationResult SignIn(String username, String password)
        {

            isAuthenticated = IsAuthenticated(username, password);

            if (!isAuthenticated)
            {
                return new AuthenticationResult("Nieprawidłowy login lub hasło");
            }

            var identity = CreateIdentity();

            authenticationManager.SignOut(MyAuthentication.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, identity);


            return new AuthenticationResult();
        }


        private ClaimsIdentity CreateIdentity()
        {
            var identity = new ClaimsIdentity(MyAuthentication.ApplicationCookie, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            identity.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "Active Directory"));
            identity.AddClaim(new Claim(ClaimTypes.Name,Settings.SuperAdminLogin));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, Settings.SuperAdminLogin));

            identity.AddClaim(new Claim(ClaimTypes.Role, Settings.SuperAdminGroupNameLocal));

            return identity;
        }

        private bool IsAuthenticated(string username, string pwd)
        {
            try
            {
                if (Settings.SuperAdminPassword == pwd && Settings.SuperAdminLogin.ToLower() == username.ToLower())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}