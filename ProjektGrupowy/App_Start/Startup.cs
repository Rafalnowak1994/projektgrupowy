﻿using Hangfire;
using Hangfire.MySql;
using Microsoft.Owin;
using Owin;


[assembly: OwinStartupAttribute(typeof(ProjektGrupowy.Startup))]
namespace ProjektGrupowy
{

    public partial class Startup
    {

        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseStorage(new MySqlStorage("MySQLContext"));
            ConfigureAuth(app);

            app.UseHangfireServer();
        }

    }
}