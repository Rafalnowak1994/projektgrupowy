﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjektGrupowy
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                      name: "LoginRoute",
                      url: "zaloguj",
                      defaults: new { controller = "Account", action = "Login" }
                    );

            routes.MapRoute(
               name: "AdminPanel",
               url: "panel",
               defaults: new { controller = "Account", action = "LocalLogin" }
                    );

            routes.MapRoute(
               name: "ServerSettings",
               url: "ustawienia",
               defaults: new { controller = "Home", action = "ChangeSettings" }
                    );

            routes.MapRoute(
                     name: "ChangePasswordRoute",
                     url: "zmiana_hasla",
                     defaults: new { controller = "UserPanel", action = "ChangePassword" }
                 );

            routes.MapRoute(
                     name: "Default",
                     url: "{controller}/{action}/{id}",
                     defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                 );
        }
    }
}

