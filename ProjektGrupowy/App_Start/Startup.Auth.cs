﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System.Web.Mvc;
using System.Web;
using ProjektGrupowy.Infrastructure;
using Hangfire;

namespace ProjektGrupowy
{
    public static class MyAuthentication
    {
        public const String ApplicationCookie = "MyProjectAuthenticationType";
    }

    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            // need helper object to generate Url from route  
            UrlHelper helper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            // need to add UserManager into owin, because this is used in cookie invalidation
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = MyAuthentication.ApplicationCookie,
                LoginPath = new PathString(helper.RouteUrl("LoginRoute")),
                Provider = new CookieAuthenticationProvider(),
                CookieName = "__AUTH",
                CookieHttpOnly = true,
                ExpireTimeSpan = TimeSpan.FromMinutes(30),
                ReturnUrlParameter = "temp"
            });

            app.UseHangfireDashboard(Settings.HangfirePath, new DashboardOptions
            {
                Authorization = new[] { new HangFireAuthorizationFilter()}
            });
        }
    }
}