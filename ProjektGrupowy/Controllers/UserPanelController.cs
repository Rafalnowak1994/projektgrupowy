﻿using ProjektGrupowy.ViewModels;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Web;
using System.Web.Mvc;

namespace ProjektGrupowy.Controllers
{
    public class UserPanelController : Controller
    {

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (User.IsInRole(Settings.SuperAdminGroupNameLocal))
            {
                Settings.SuperAdminPassword = model.NewPassword;
            }
            else
            {
                var userName = HttpContext.GetOwinContext().Authentication.User.Identity.Name;
                using (PrincipalContext principalContext = Settings.GetPrincipalContext())
                {
                    using (var user = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, userName))
                    {
                        user.SetPassword(model.NewPassword);
                    }
                }
            }

            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult ChangeData()
        {
            var userName = HttpContext.GetOwinContext().Authentication.User.Identity.Name;
            ChangeDataViewModel model = new ChangeDataViewModel();

            using (PrincipalContext principalContext = Settings.GetPrincipalContext())
            {
                using (var user = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, userName))
                {

                    model.Email = user.EmailAddress;
                    model.Name = user.GivenName;
                    model.Surname = user.Surname;
                    DirectoryEntry entry = (DirectoryEntry)user.GetUnderlyingObject();
                    string zgody;
                    if (entry.Properties["postalCode"].Value == null)
                    {
                        zgody = null;
                    }
                    else {
                        zgody = entry.Properties["postalCode"].Value.ToString();
                    }
                    if (!(string.IsNullOrEmpty(zgody)))
                    {
                        if (entry.Properties["postalCode"].Value.ToString().Contains("GIODO") && entry.Properties["postalCode"].Value.ToString().Contains("Newsletter"))
                        {
                            var info = entry.Properties["postalCode"].Value.ToString().Split(';');
                            var giodo = info[0].Split(':');
                            var newsletter = info[1].Split(':');
                            if (giodo[1] == "0")
                            { model.GIODO = false; }
                            else { model.GIODO = true; }
                            if (newsletter[1] == "0")
                            { model.Newsletter = false; }
                            else { model.Newsletter = true; }
                        }
                        else
                        {
                            model.GIODO = false;
                            model.Newsletter = false;

                        }
                    }
                    else
                    {
                        model.GIODO = false;
                        model.Newsletter = false;
                    }

                }
            }
            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeData(ChangeDataViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            var userName = HttpContext.GetOwinContext().Authentication.User.Identity.Name;
            using (PrincipalContext principalContext = Settings.GetPrincipalContext())
            {
                using (var user = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, userName))
                {
                   
                    user.EmailAddress = model.Email;
                    user.GivenName = model.Name;
                    user.Surname = model.Surname;
                    DirectoryEntry entry = (DirectoryEntry)user.GetUnderlyingObject();
                    string zgody;
                    if (entry.Properties["postalCode"].Value == null)
                    {
                        zgody = null;
                    }
                    else
                    {
                        zgody = entry.Properties["postalCode"].Value.ToString();
                    }
                    if (!(string.IsNullOrEmpty(zgody)))
                    {
                        if (entry.Properties["postalCode"].Value.ToString().Contains("GIODO") && entry.Properties["postalCode"].Value.ToString().Contains("Newsletter"))
                        {
                            var info = entry.Properties["postalCode"].Value.ToString().Split(';');
                            entry.Properties["postalCode"].Value = model.GIODO ? "GIODO:1;" : "GIODO:0;";
                            entry.Properties["postalCode"].Value += model.Newsletter ? "Newsletter:1;" : "Newsletter:0;";
                            if (info.Length > 2)
                            {
                                for (int i = 2; i < info.Length; i++)
                                {
                                    entry.Properties["postalCode"].Value += info[i];
                                }
                            }
                        }
                        else
                        {
                            var info = entry.Properties["postalCode"].Value.ToString();
                            entry.Properties["postalCode"].Value = model.GIODO ? "GIODO:1;" : "GIODO:0;";
                            entry.Properties["postalCode"].Value += model.Newsletter ? "Newsletter:1;" : "Newsletter:0;";
                            entry.Properties["postalCode"].Value += info;
                        }
                    }
                    else
                    {
                        entry.Properties["postalCode"].Value = model.GIODO ? "GIODO:1;" : "GIODO:0;";
                        entry.Properties["postalCode"].Value += model.Newsletter ? "Newsletter:1;" : "Newsletter:0;";
                    }
                    user.Save();
                    entry.CommitChanges();
                }
            }
            return View(model);
        }

    }
}