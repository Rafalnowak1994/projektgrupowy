﻿using Hangfire;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using NLog;
using ProjektGrupowy.DAL;
using ProjektGrupowy.Infrastructure;
using ProjektGrupowy.Models;
using ProjektGrupowy.ViewModels;
using System;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProjektGrupowy.Controllers
{
    public class AccountController : Controller
    {
        private MySQLContext mySQLContext { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private IAuthenticationManager AuthenticatManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }

        }

        private bool validatereCAPTCHA(string response)
        {
            try
            {
                string reCAPTCHASecretKey = Settings.reCAPTCHASecretKey;
                var clinet = new WebClient();
                var result = clinet.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", reCAPTCHASecretKey, response));
                var obj = JObject.Parse(result);
                return (bool)obj.SelectToken("success");
            }
            catch
            {
                return false;
            }
        }

        public ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult SuccessMessage()
        {
            if (TempData["SuccessMessage"] != null)
            {
                ViewBag.SuccessMessage = TempData["SuccessMessage"];
                return View("_SuccessMessage");
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult ErrorMessage(string returnUrl)
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
                return View("_ErrorMessage");
            }
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult LocalLogin()
        {
            if (Settings.ShowConfigSite == "1")
                return RedirectToAction("FirstConfig");

            if (!Request.IsAuthenticated)
                return View();

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LocalLogin(LocalLoginViewModel model, string temp)
        {

            // usually this will be injected via DI. but creating this manually now for brevity
            var authService = new LocalAuthenticationService(AuthenticatManager);
            var authenticationResult = authService.SignIn(model.Username, model.Password);

            if (authenticationResult.IsSuccess)
            {
                logger.Info($"Użytkownik {model.Username} zalogował się do serwisu");

                return RedirectToAction("Index", "Home");
            }

            ModelState.AddModelError("", authenticationResult.ErrorMessage);
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Login(string temp)
        {
            ViewBag.temp = temp;

            if (Settings.ShowConfigSite == "1")
                return RedirectToAction("FirstConfig");

            if (!Request.IsAuthenticated)
                return View();

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string temp)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // usually this will be injected via DI. but creating this manually now for brevity
            var authService = new ADAuthenticationService(AuthenticatManager);
            var authenticationResult = authService.SignIn(model.Username, model.Password);

            if (authenticationResult.IsSuccess)
            {
                Session["UserHasDefaultPassword"] = false;

                logger.Info($"Użytkownik {model.Username} zalogował się do serwisu");

                return RedirectToLocal(temp);
            }

            ModelState.AddModelError("", authenticationResult.ErrorMessage);
            return View(model);
        }

        [ValidateAntiForgeryToken]
        public ActionResult Logoff()
        {
            Session["UserHasDefaultPassword"] = null;
            AuthenticatManager.SignOut(MyAuthentication.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult FirstConfig()
        {
            if (Settings.ShowConfigSite == "0")
                return RedirectToAction("Index", "Home");
            else
                return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult FirstConfig(FirstConfigViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (Settings.ShowConfigSite != "0")
            {
                Settings.SuperAdminLogin = model.Username.ToLower().Trim();
                Settings.SuperAdminPassword = model.Password;
                Settings.ADDomian = model.ADDomian;
                Settings.ShowConfigSite = "0";
                logger.Info("Zapisano wstępną konfigurację");
            }

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult CreateAccount()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAccount(CreateAccountViewModel model)
        {
            //get captcha validate status
            var captchaStatus = validatereCAPTCHA(Request["g-recaptcha-response"]);

            //check GIODO
            if (!model.GIODO)
            {
                ModelState.AddModelError("", "Musisz zatwierdzić zgodę przetwarzania danych");
                return View();
            }

            //check model and captcha status
            if (!ModelState.IsValid || !captchaStatus)
            {
                if (!captchaStatus)
                    ModelState.AddModelError("", "Bład walidacji captcha");
                return View();
            }

            try
            {
                using (PrincipalContext principalContext = Settings.GetPrincipalContext())
                {
                    // Check if user object already exists in the store
                    UserPrincipal usr = UserPrincipal.FindByIdentity(principalContext, $"s{model.Index.ToString()}");
                    if (usr != null)
                    {
                        ModelState.AddModelError("Index", "Konto o podanym indeksie " + model.Index + " już istnieje");
                        return View(model);
                    }
                }

                //if user has token
                if (model.HaveToken)
                {
                    using (mySQLContext = new MySQLContext())
                    {
                        var token = mySQLContext.UserRegisterTokens.Where(w => (w.Token == model.Token) && (w.Expire > DateTime.Now)).FirstOrDefault();

                        if (token != null)
                        {
                            if (ADUserMethods.addUserToActiveDirectory(model))
                            {
                                logger.Info($"Użytkownik o indeksie {model.Index} i o adresie email {model.Email}" +
                                    $" założył konto.");

                                //delete used token form database
                                mySQLContext.UserRegisterTokens.Remove(token);
                                mySQLContext.SaveChanges();


                                //send link to change password
                                string passwordRecoverytoken = ADUserMethods.AddPasswordRecoveryTokenToDataBase($"s{model.Index}");
                                if (passwordRecoverytoken != "")
                                {
                                    //send email in background
                                    BackgroundJob.Enqueue(() => MailSender.sendNewTokenToPasswordRecovery(Url.Action("PasswordRecoverySecondStep", "Account", new { token = passwordRecoverytoken }, Request.Url.Scheme), model.Email, $"s{model.Index}"));
                                }

                                TempData["SuccessMessage"] = $"Twoje konto zostało stworzone pomyślnie. Na adres {model.Email} został wysłany link do ustawienia hasła.";
                                return RedirectToAction("SuccessMessage");
                            }
                            else
                            {
                                TempData["ErrorMessage"] = "Wystąpił błąd podczas tworzenia konta";
                                return RedirectToAction("ErrorMessage");
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("Token", "Brak podanego tokena w bazie");
                            return View(model);
                        }
                    }
                }


                using (mySQLContext = new MySQLContext())
                {

                    if (mySQLContext.Users.Where(w => w.Email == model.Email || w.Index == model.Index).FirstOrDefault() != null)
                    {
                        ModelState.AddModelError("", "Konto o podanym adresie lub indeksie czeka na zatwierdzenie");
                        return View(model);
                    }

                    User user = new User
                    {
                        Email = model.Email,
                        Name = model.Name,
                        Surname = model.Surname,
                        GIODO = model.GIODO,
                        Newsletter = model.Newsletter,
                        Confirmed = false,
                        Index = model.Index,
                        CreatedDate = DateTime.Now
                    };

                    mySQLContext.Users.Add(user);
                    mySQLContext.SaveChanges();
                }

                TempData["SuccessMessage"] = "Twoje konto zostało stworzone pomyślnie i czeka na aktywację";
                if (Settings.InfoMail != null && Settings.InfoMail != "")
                {//send email in background
                    BackgroundJob.Enqueue(() => MailSender.sendMessage(Settings.InfoMail, "Aktywuj konto", $"Konto uzytkownika s{model.Index} czeka na aktywacje"));
                }
                return RedirectToAction("SuccessMessage");
            }


            catch (Exception e)
            {
                logger.Warn(e, "Wystąpił błąd zapisu konta użytkowanika do bazy danych");

                TempData["ErrorMessage"] = "Wystąpił błąd podczas tworzenia konta";
                return RedirectToAction("ErrorMessage");
            }
        }

        [AllowAnonymous]
        public ActionResult PasswordRecoverySecondStep(string token)
        {
            using (mySQLContext = new MySQLContext())
            {
                var passwordRecoveryToken = mySQLContext.PasswordRecoveryTokens.Where(w => w.Token == token).FirstOrDefault();

                if (passwordRecoveryToken == null)
                {
                    TempData["ErrorMessage"] = "Podany token jest nieprawidłowy!";
                    return RedirectToAction("ErrorMessage");
                }
                else if (passwordRecoveryToken.Expire < DateTime.Now)
                {
                    TempData["ErrorMessage"] = "Podany token stracił ważność!";
                    return RedirectToAction("ErrorMessage");
                }
                else
                {
                    TempData["Token"] = token;
                }
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordRecoverySecondStep(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (ADUserMethods.ChangePassword(model.NewPassword, TempData["Token"]?.ToString() ?? ""))
            {
                TempData["SuccessMessage"] = "Twoje hasło zostało zmienione";
                return RedirectToAction("SuccessMessage");
            }

            else
            {
                TempData["ErrorMessage"] = "Błąd zmainy hasła. Proszę o kontakt z administratorem";
                logger.Fatal($"Błąd zmiany hasła");
                return RedirectToAction("ErrorMessage");
            }
        }

        [AllowAnonymous]
        public ActionResult PasswordRecoveryFirstStep()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordRecoveryFirstStep(PasswordRecoveryFirstStepViewModel model)
        {

            //get captcha validate status
            var captchaStatus = validatereCAPTCHA(Request["g-recaptcha-response"]);

            if (!ModelState.IsValid || !captchaStatus)
            {
                if (!captchaStatus)
                    ModelState.AddModelError("", "Bład walidacji captcha");
                return View();
            }

            using (PrincipalContext principalContext = Settings.GetPrincipalContext())
            {
                // Check if user object already exists in the store
                UserPrincipal user = UserPrincipal.FindByIdentity(principalContext, model.Login);
                if (user == null)
                {
                    ModelState.AddModelError("Login", "Konto " + model.Login + " nie istnieje");
                }
                else if (user.EmailAddress == null)
                {
                    ModelState.AddModelError("Login", "Brak przyłączonego adresu email");
                }
                else
                {

                    string passwordRecoverytoken = ADUserMethods.AddPasswordRecoveryTokenToDataBase(model.Login);
                    if (passwordRecoverytoken != "")
                    {

                        //send email in background
                        BackgroundJob.Enqueue(() => MailSender.sendNewTokenToPasswordRecovery(Url.Action("PasswordRecoverySecondStep", "Account", new { token = passwordRecoverytoken }, Request.Url.Scheme), user.EmailAddress, user.SamAccountName));
                        TempData["SuccessMessage"] = "Wiadomość z linkiem do zmiany hasła została wysłana na adres e-mail podany przy rejestracji";
                        return RedirectToAction("SuccessMessage");
                    }

                    else
                    {
                        TempData["ErrorMessage"] = "Błąd zmainy hasła. Proszę o kontakt z administratorem";
                        return RedirectToAction("ErrorMessage");
                    }
                }

                return View();
            }
        }
    }
}