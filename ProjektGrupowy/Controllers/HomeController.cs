﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjektGrupowy.Infrastructure;
using ProjektGrupowy.ViewModels;
using Microsoft.Owin.Security;
using NLog;
using System.Security.Claims;
using System.DirectoryServices.AccountManagement;
using ProjektGrupowy.Models;
using ProjektGrupowy.DAL;
using Hangfire;
using System.Data;
using LinqToExcel;
using OfficeOpenXml;
using System.IO;
using System.Web.Services;
using static ProjektGrupowy.ViewModels.DataGridsViewModels;

namespace ProjektGrupowy.Controllers
{
    public class HomeController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static Random random = new Random();
        private static MySQLContext mySQLContext;
        // GET: Home
        private IAuthenticationManager AuthenticatManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }

        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public FileResult createExcel(int Id)
        {
            List<ExcelItem> excelItems = new List<ExcelItem>();
            using (mySQLContext = new MySQLContext())
            {
                foreach (var item in mySQLContext.UserRegisterTokens.Where(w => w.GroupId == Id).ToList())
                {
                    excelItems.Add(new ExcelItem { Token = item.Token, WaznyDo = item.Expire.ToLongDateString() });
                }

            }

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            workSheet.Cells[1, 1].LoadFromCollection(excelItems, true);
            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=Tokeny.xlsx");
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();

                return File(memoryStream, "Excel", "Excel");
            }
        }



        public ActionResult SuccessMessage()
        {
            if (TempData["SuccessMessage"] != null)
            {
                ViewBag.SuccessMessage = TempData["SuccessMessage"];
                return View("_SuccessMessage");
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult ErrorMessage(string returnUrl)
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
                return View("_ErrorMessage");
            }
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }


        [Authorize]
        [HttpPost]
        public ActionResult SendTokens(EmailsTokenViewModel model)
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.SendTokensGroup)
              || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {

                string[] emails = model.Mails.Split(',');
                DateTime dateExpire = DateTime.Now;
                dateExpire = dateExpire.AddDays(model.Days);
                List<string> badEmails = new List<string>();
                List<string> goodEmails = new List<string>();
                try
                {
                    for (int i = 0; i < emails.Count(); i++)
                    {
                        if (IsValidEmail(emails[i].Trim()))
                        {
                            goodEmails.Add(emails[i].Trim());
                            BackgroundJob.Enqueue(() => MailSender.addTokenToDataBaseAndSend(emails[i], dateExpire));
                        }

                        else
                        {
                            badEmails.Add(emails[i].Trim());
                            emails[i] = null;
                        }
                    }

                    if (goodEmails.Count != 0)
                    {

                        TempData["Message"] = $"Wysłano tokeny na {String.Join(", ", goodEmails)}.";
                        TempData["Type"] = "success";

                        if (badEmails.Count != 0)
                        {
                            TempData["Message"] += $"Podane adresy {String.Join(", ", badEmails)} mają zły format i zostały odrzucone.";
                            TempData["Type"] = "warning";
                        }
                    }
                    else
                    {
                        TempData["Message"] = $"Brak adresów o prawidłowym formacie. Popraw podane adresy {String.Join(", ", badEmails)}.";
                        TempData["Type"] = "warning";
                    }
                    return View("TokenGenerate");
                }
                catch (Exception e)
                {
                    TempData["Message"] = $"Błąd wysyłania tokenów";
                    TempData["Type"] = "danger";
                    logger.Fatal($"Błąd wysyłania tokenów {e}");
                    return View("TokenGenerate");
                }
            }
            else
                return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult DeleteTokenFromGroup(int Id)
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.SendTokensGroup)
              || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {

                using (mySQLContext = new MySQLContext())
                {
                    var groupToDelete = mySQLContext.TokenGroups.Where(w => w.Id == Id).FirstOrDefault();
                    mySQLContext.TokenGroups.Remove(groupToDelete);

                    foreach (var token in mySQLContext.UserRegisterTokens.Where(w => w.GroupId == Id))
                    {
                        mySQLContext.UserRegisterTokens.Remove(token);
                    }

                    mySQLContext.SaveChanges();
                }
                return View("TokenGenerate");
            }
            else
                return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult TokenGenerate()
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.SendTokensGroup)
              || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {
                return View();
            }
            else
                return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult ActivateUserFromDataBase()
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.ActivateUsersFromDataBaseGroup)
              || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {
                return View();
            }
            else
                return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TokenGroupGenerate(TokenGroupGenerateViewModel model)
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.SendTokensGroup)
              || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {
                if (!ModelState.IsValid)
                    return View("TokenGenerate");

                using (mySQLContext = new MySQLContext())
                {
                    DateTime dateExpire = DateTime.Now;
                    dateExpire = dateExpire.AddDays(model.Days);
                    var tokenGroup = new TokenGroup { Expire = dateExpire, Name = model.Name };
                    mySQLContext.TokenGroups.Add(tokenGroup);
                    mySQLContext.SaveChanges();

                    for (int i = 0; i < model.Amount; i++)
                    {
                        mySQLContext.UserRegisterTokens.Add(new UserRegisterToken { Expire = dateExpire, Token = ADUserMethods.GenerateToken(), GroupId = tokenGroup.Id });
                    }
                    mySQLContext.SaveChanges();
                }
            }

            return View("TokenGenerate");
        }



        [Authorize]
        public ActionResult UserEditGrid()
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.EditUsersGroup)
              || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {
                return View();
            }
            else
                return RedirectToAction("Index");
        }

        [WebMethod]
        public void DeleteUsers(DeleteUsers param)
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.EditUsersGroup)
            || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {
                try
                {
                    using (PrincipalContext ctx = Settings.GetPrincipalContext())
                    {
                        UserPrincipal user;
                        foreach (var item in param.Logins)
                        {
                            user = UserPrincipal.FindByIdentity(ctx, item);

                            if (user != null)
                            {
                                user.Delete();
                            }
                        }
                    }
                }
                catch
                {

                }
            }
        }

        [WebMethod]
        public void DeleteUsersToActivateFromDataBase(UsersIDsFromDataBases param)
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.EditUsersGroup)
            || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {
                try
                {
                    using (mySQLContext = new MySQLContext())
                    {
                        foreach (var item in param.IDs)
                        {
                            var id = Int32.Parse(item);
                            var itemToDelete = mySQLContext.Users.Where(w => w.Id == id).FirstOrDefault();
                            mySQLContext.Users.Remove(itemToDelete);
                            mySQLContext.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        [WebMethod]
        public void ActivateSelectedUserFromDataBase(UsersIDsFromDataBases param)
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.EditUsersGroup)
            || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {
                try
                {
                    using (mySQLContext = new MySQLContext())
                    {
                        foreach (var item in param.IDs)
                        {
                            var id = Int32.Parse(item);
                            var itemToDelete = mySQLContext.Users.Where(w => w.Id == id).FirstOrDefault();

                            if (ADUserMethods.addUserToActiveDirectory(new CreateAccountViewModel
                            {
                                Email = itemToDelete.Email,
                                Name = itemToDelete.Name,
                                Surname = itemToDelete.Surname,
                                Index = itemToDelete.Index,
                                GIODO = itemToDelete.GIODO,
                                Newsletter = itemToDelete.Newsletter
                            }
                            ))
                            {
                                logger.Info($"Konto użytkowanika o indeksie {itemToDelete.Index} zostało założone.");

                                //send link to change password
                                string passwordRecoverytoken = ADUserMethods.AddPasswordRecoveryTokenToDataBase($"s{itemToDelete.Index}");
                                if (passwordRecoverytoken != "")
                                {
                                    //send email in background
                                    BackgroundJob.Enqueue(() => MailSender.sendNewTokenToPasswordRecovery(Url.Action("PasswordRecoverySecondStep", "Account", new { token = passwordRecoverytoken }, Request.Url.Scheme), itemToDelete.Email, $"s{itemToDelete.Index}"));
                                }
                            }

                            mySQLContext.Users.Remove(itemToDelete);
                            mySQLContext.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        public JsonResult LoadTokenGroups(DTParameters param)
        {
            try
            {
                List<TokenGroupDataTable> tokenGroups = new List<TokenGroupDataTable>();
                using (mySQLContext = new MySQLContext())
                {
                    foreach (var item in mySQLContext.TokenGroups.Where(w => w.Expire > DateTime.Now))
                    {
                        tokenGroups.Add(new TokenGroupDataTable { Name = item.Name, Date = item.Expire.ToString(), Id = item.Id.ToString() });
                    }
                }

                DTResult<TokenGroupDataTable> result = new DTResult<TokenGroupDataTable>
                {
                    draw = param.Draw,
                    data = tokenGroups,
                    recordsFiltered = tokenGroups.Count,
                    recordsTotal = tokenGroups.Count
                };
                return Json(result);

            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult LoadUsersFromAD(DTParameters param)
        {
            try
            {
                var usersFromAD = new List<UsersTableViewModel>();
                using (var context = Settings.GetPrincipalContext())
                {
                    using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                    {
                        int counter = 1;
                        foreach (var _result in searcher.FindAll())
                        {
                            DirectoryEntry de = _result.GetUnderlyingObject() as DirectoryEntry;

                            usersFromAD.Add(new UsersTableViewModel
                            {

                                Id = counter.ToString(),
                                Login = de.Properties["samAccountName"].Value.ToString(),
                                Email = (de.Properties["mail"].Value == null) ? "" : de.Properties["mail"].Value.ToString(),
                                Description = (de.Properties["description"].Value == null) ? "" : de.Properties["description"].Value.ToString(),
                                GIODO = (de.Properties["postalCode"].Value == null) ? "" : de.Properties["postalCode"].Value.ToString(),
                                Expire = UserPrincipal.FindByIdentity(context, de.Properties["samAccountName"].Value.ToString()).AccountExpirationDate.ToString()

                            });

                            counter++;
                        }
                    }
                }

                List<String> columnSearch = new List<string>();

                foreach (var col in param.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                List<UsersTableViewModel> data = new DataTableResultSet().GetResult(param.Search.Value, param.SortOrder, param.Start, param.Length, usersFromAD, columnSearch);
                int count = new DataTableResultSet().Count(param.Search.Value, usersFromAD, columnSearch);

                DTResult<UsersTableViewModel> result = new DTResult<UsersTableViewModel>
                {
                    draw = param.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);

            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult LoadUsersFromDataBaseToActivate(DTParameters param)
        {
            try
            {
                var usersToActivate = new List<UsersToActivateFromDataBaseViewModel>();

                using (mySQLContext = new MySQLContext())
                {
                    int counter = 1;
                    foreach (var item in mySQLContext.Users.ToList())
                    {

                        usersToActivate.Add(new UsersToActivateFromDataBaseViewModel
                        {
                            Id = item.Id.ToString(),
                            Index = item.Index.ToString(),
                            Date = item.CreatedDate.ToString(),
                            Email = item.Email,
                            Name = item.Name,
                            Surname = item.Surname,
                            Description = (item.GIODO ? "GIODO:1;" : "GIODO:0;") + (item.Newsletter ? "Newsletter:1;" : "Newsletter:0;")
                        });

                        counter++;
                    }
                }


                List<String> columnSearch = new List<string>();

                foreach (var col in param.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                List<UsersToActivateFromDataBaseViewModel> data = new DataTableResultSet().GetResult(param.Search.Value, param.SortOrder, param.Start, param.Length, usersToActivate, columnSearch);
                int count = new DataTableResultSet().Count(param.Search.Value, usersToActivate, columnSearch);

                DTResult<UsersToActivateFromDataBaseViewModel> result = new DTResult<UsersToActivateFromDataBaseViewModel>
                {
                    draw = param.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [Authorize]
        public ActionResult ChangeSettings()
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.ChangeSettingsGroup)
               || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {
                SettingsViewModel model = new SettingsViewModel
                {
                    ADDomian = Settings.ADDomian,
                    HangfirePath = Settings.HangfirePath,
                    ChangeSettingsGroup = Settings.ChangeSettingsGroup,
                    SendTokensGroup = Settings.SendTokensGroup,
                    EditUsersGroup = Settings.EditUsersGroup,
                    HomeDirectory = Settings.HomeDirectory,
                    HomeDrive = Settings.HomeDrive,
                    MsSFU30NisDomain = Settings.MsSFU30NisDomain,
                    LoginShell = Settings.LoginShell,
                    DefaultUserGroupName = Settings.DefaultUserGroupName,
                    UidNumber = Settings.UidNumber,
                    GidNumber = Settings.GidNumber,
                    Unixhomedirectory = Settings.Unixhomedirectory,
                    ActivateUsersFromDataBaseGroup = Settings.ActivateUsersFromDataBaseGroup,
                    Descripton = Settings.Description,
                    ProfilePath = Settings.ProfilePath,
                    InfoMail = Settings.InfoMail
                };
                return View(model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeSettings(SettingsViewModel model)
        {
            if (ClaimsPrincipal.Current.IsInRole(Settings.ChangeSettingsGroup)
               || ClaimsPrincipal.Current.IsInRole(Settings.SuperAdminGroupNameLocal))
            {

                if (!ModelState.IsValid)
                    return View(model);

                Settings.ADDomian = model.ADDomian.Trim();
                Settings.HangfirePath = model.HangfirePath.Trim();
                Settings.ChangeSettingsGroup = model.ChangeSettingsGroup.Trim();
                Settings.SendTokensGroup = model.SendTokensGroup.Trim();
                Settings.EditUsersGroup = model.EditUsersGroup.Trim();
                Settings.HomeDirectory = model.HomeDirectory;
                Settings.HomeDrive = model.HomeDrive;
                Settings.MsSFU30NisDomain = model.MsSFU30NisDomain;
                Settings.LoginShell = model.LoginShell;
                Settings.DefaultUserGroupName = model.DefaultUserGroupName;
                Settings.UidNumber = model.UidNumber;
                Settings.GidNumber = model.GidNumber;
                Settings.Unixhomedirectory = model.Unixhomedirectory;
                Settings.ActivateUsersFromDataBaseGroup = model.ActivateUsersFromDataBaseGroup;
                Settings.Description = model.Descripton;
                Settings.ProfilePath = model.ProfilePath;
                Settings.InfoMail = model.InfoMail; 
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [WebMethod]
        public ActionResult ModifyUsers(ModifyUsersViewModel model)
        {
            if (model.Logins.Length > 0)
            {

                using (PrincipalContext principalContext = Settings.GetPrincipalContext())
                {
                    foreach (var login in model.Logins)
                    {
                        using (var user = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, login))
                        {
                            if (!String.IsNullOrEmpty(model.HomeDirectory))
                            {
                                user.HomeDirectory = model.HomeDirectory + user.SamAccountName;
                            }
                            if (!String.IsNullOrEmpty(model.HomeDrive))
                            {
                                user.HomeDirectory = model.HomeDrive;
                            }
                            user.Save();
                            DirectoryEntry entry = (DirectoryEntry)user.GetUnderlyingObject();
                            if (!String.IsNullOrEmpty(model.MsSFU30NisDomain))
                            {
                                entry.Properties["msSFU30NisDomain"].Value = model.MsSFU30NisDomain;
                            }
                            if (!String.IsNullOrEmpty(model.LoginShell))
                            {
                                entry.Properties["LoginShell"].Value = model.LoginShell;
                            }
                            if (!String.IsNullOrEmpty(model.GidNumber))
                            {
                                entry.Properties["gidNumber"].Value = model.GidNumber;
                            }
                            if (!String.IsNullOrEmpty(model.Unixhomedirectory))
                            {
                                entry.Properties["unixhomedirectory"].Value = model.Unixhomedirectory + user.SamAccountName;
                            }
                            entry.CommitChanges();
                        }


                    }
                }

            }
            return RedirectToAction("UserEditGrid", "Home");
        }
        public JsonResult GetADGroups()
        {

            List<string> aDGroupNames = new List<string>();
            PrincipalContext ctx = Settings.GetPrincipalContext();
            GroupPrincipal qbeGroup = new GroupPrincipal(ctx);
            PrincipalSearcher srch = new PrincipalSearcher(qbeGroup);
            foreach (var found in srch.FindAll())
            {
                aDGroupNames.Add(found.Name);
            }


            return Json(aDGroupNames);
        }
        public ActionResult AddUserToGroup(AddUserToGroupViewModel model)
        {
            using (PrincipalContext ctx = Settings.GetPrincipalContext())
            {
                GroupPrincipal group = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, model.ADGroups);
                bool isingrp = false;
                foreach (var login in model.Logins)
                {
                    UserPrincipal user = UserPrincipal.FindByIdentity(ctx, login);
                  
                    foreach(var loginek in group.GetMembers())
                    {
                        if (loginek.ToString() == user.SamAccountName.ToString())
                        {
                            isingrp = true;
                        }
                    }
                    var ismember = user.IsMemberOf(group);
                    if (!(isingrp))
                    {
                        group.Members.Add(ctx, IdentityType.UserPrincipalName, user.UserPrincipalName);
                        group.Save();
                    }
                }

            }
            return RedirectToAction("UserEditGrid");
        }
        public ActionResult SendMSG(SendMSGViewModel model)
        {

            //send email in background
            foreach (var item in model.Emails)
            {
                BackgroundJob.Enqueue(() => MailSender.sendMessage(item, model.subject, model.message));
            }
            return RedirectToAction("UserEditGrid");
        }
    }
}

